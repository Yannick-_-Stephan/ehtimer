//
//  EasyTimer.swift
//  EasyHelper
//
//  Created by DaRk-_-D0G on 16/10/2015.
//  Copyright © 2015 DaRk-_-D0G. All rights reserved.
//

import Foundation

/// Class for Timer / Chrono
public class EHTimer {
    
    
    private var startTime:CFAbsoluteTime?
    private var endTime:CFAbsoluteTime?
    // Add Timer
    public var elapsedTimeSet:Set<CFAbsoluteTime>?
    
    /// time elapsed / Duration Current - Start
    public var durationElapsed:CFAbsoluteTime? {
        guard let hasStartTime = self.startTime else {
            print("Duration haven't startTime (Start the EasyTimer)")
            return nil
        }
        return CFAbsoluteTimeGetCurrent() - hasStartTime
    }
    
    public init() { }
    /**
     Deinit
     */
    deinit {
        self.startTime = nil
        self.endTime = nil
    }
    /**
     Start Timer
     
     - returns: CFAbsoluteTime / Start
     */
    public func start() -> CFAbsoluteTime  {
        self.startTime = CFAbsoluteTimeGetCurrent()
        return self.startTime!
    }
    /**
     Reset Timer
     */
    public func reset() {
        self.startTime = nil
        self.endTime = nil
        self.elapsedTimeSet = nil
    }
    /**
     Stop Timer
     
     - returns: CFAbsoluteTime / End
     */
    public func stop() -> CFAbsoluteTime? {
        defer {
            self.startTime = nil
            self.endTime = nil
        }
        
        guard let hasStartTime = self.startTime else {
            print("Duration haven't startTime (Start the EasyTimer)")
            return nil
        }
        
        self.endTime = CFAbsoluteTimeGetCurrent()
        return self.endTime! - hasStartTime
    }
    /**
     Add Current timer
     */
    public func addElapsedTime() {
        defer {
            if let hasElapsed = self.durationElapsed  {
                self.elapsedTimeSet!.insert(hasElapsed)
            } else {
                print("Duration haven't startTime (Start the EasyTimer)")
            }
        }
        guard self.elapsedTimeSet != nil else {
            self.elapsedTimeSet = Set<CFAbsoluteTime>()
            return
        }
    }
    /**
     call after elapsed time
     
     - parameter completionTime: duration:CFAbsoluteTime)->()
     */
    public func callByElapsedTime(completionTime:(duration:CFAbsoluteTime)->()) {
        
        guard let hasElapsetTime = self.elapsedTimeSet else {
            print("EasyTime elapsed time (Add with addElapsedTime)")
            return
        }
        for time in hasElapsetTime {
            dispatch_sync(dispatch_get_main_queue(), {
                completionTime(duration: time)
                // Update the UI on the main thread.
            })
        }
        
        
    }
    /**
     Get duration of func
     
     - parameter f: (@autoclosure f: () -> A)
     
     - returns: (result:A, duration: CFAbsoluteTime)
     */
    public static func durationFunc <A> (@autoclosure f: () -> A) -> (result:A, duration: CFAbsoluteTime) {
        let easytimer = EHTimer()
        easytimer.start()
        let result = f()
        let valueEnd = easytimer.stop()!
        return (result, valueEnd)
    }
    
}